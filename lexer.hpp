// --- lexer.hpp ----
// A simple lexer for a slightly modified version of the Kaleidoscope language.
// Based off the LLVM Kaleidoscope tutorial found at
// https://llvm.org/docs/tutorial
//
// Uses version 5.0.1 of LLVM and Clang
//
// --- Copyright (C) Jad Elkhaleq Ghalayini, 2018 ---
// This file is distributed under the MIT License.
// See LICENSE.TXT for details

#ifndef KALEIDOSCOPE_LEXER_INCLUDED
#define KALEIDOSCOPE_LEXER_INCLUDED

#include <string>
#include <cstdio>
#include <utility>
#include <vector>

namespace kaleidoscope
{

  bool OPTIMIZATION_FLAG = true;
  bool PRINT_DEBUG_FLAG = false;
  bool PRINT_IR_FLAG = false;

  enum Token
  {
    tok_eof = -1,

    // Commands
    tok_def = -2,
    tok_extern = -3,

    // Primary
    tok_id = -4,
    tok_num = -5,

    // Character
    left_brace = '(', right_brace = ')', comma = ',', semicolon = ';',
    left_subscript = '[', right_subscript = ']',
    left_array = '{', right_array = '}',
    tok_eq = '=',
    tok_term = ';',

    // Control
    tok_if = -6,
    tok_then = -7,
    tok_else = -8,
    tok_for = -9,
    tok_do = -10,

    // Operator
    tok_unary = -11,
    tok_binary = -12,

    // Storage
    tok_let = -13,
    tok_in = -14,
    tok_ref = -15,
    tok_vec = -16,

    // Error
    tok_err = -17

  };

  Token op_token(char op)
  {
    return static_cast<Token>(op);
  }

  class Lexer
  {
    FILE* source;
    std::string id;
    double val;
    int last;
    Token curr_tok;
    std::vector<FILE*> file_stack;

  public:
    Lexer(FILE* src): source(src), id(), val(0), last(' '),
    curr_tok(static_cast<Token>(0)) {}
    Lexer(): source(stdin), id(), val(0), last(' '),
    curr_tok(static_cast<Token>(0)) {}

    inline void read(FILE* src)
    {
      source = src;
    }

    inline std::string get_id() const
    {
      return id;
    }

    inline double get_val() const
    {
      return val;
    }

    inline int get_last() const
    {
      return last;
    }

    inline FILE* get_input() const
    {
      return source;
    }

    inline FILE* set_input(FILE* input)
    {
      return source = input;
    }

    Token get_token()
    {
      return get_token(source);
    }

    Token get_token(FILE* input)
    {
      while(isspace(last)) // Check for whitespace (and ignore)
      {
        last = getc(input);
      }
      if(isalpha(last)) // Check for IDs
      {
        id = last;
        while(isalnum(last = getc(input)))
        {
          id += last;
        }

        if(id == "def")
        {
          return tok_def;
        }
        else if(id == "extern")
        {
          return tok_extern;
        }
        else if(id == "if")
        {
          return tok_if;
        }
        else if(id == "then")
        {
          return tok_then;
        }
        else if(id == "else")
        {
          return tok_else;
        }
        else if(id == "for")
        {
          return tok_for;
        }
        else if(id == "do")
        {
          return tok_do;
        }
        else if(id == "in")
        {
          return tok_in;
        }
        else if(id == "ref")
        {
          return tok_ref;
        }
        else if(id == "vec")
        {
          return tok_vec;
        }
        else if(id == "binary")
        {
          return tok_binary;
        }
        else if(id == "unary")
        {
          return tok_unary;
        }
        else if(id == "let")
        {
          return tok_let;
        }
        else if(id == "QUIT")
        {
          std::exit(0);
        }
        else if(id == "OPTIMIZE")
        {
          OPTIMIZATION_FLAG = true;
          if(input == stdin)
          {
            printf("ready>>> ");
          }
          return get_token();
        }
        else if(id == "DONTOPTIMIZE")
        {
          OPTIMIZATION_FLAG = false;
          if(input == stdin)
          {
            printf("ready>>> ");
          }
          return get_token();
        }
        else if(id == "DEBUG")
        {
          PRINT_DEBUG_FLAG = true;
          if(input == stdin)
          {
            printf("ready>>> ");
          }
          return get_token();
        }
        else if(id == "DONTDEBUG")
        {
          PRINT_DEBUG_FLAG = false;
          if(input == stdin)
          {
            printf("ready>>> ");
          }
          return get_token();
        }
        else if(id == "PRINT")
        {
          PRINT_IR_FLAG = true;
          if(input == stdin)
          {
            printf("ready>>> ");
          }
          return get_token();
        }
        else if(id == "DONTPRINT")
        {
          PRINT_IR_FLAG = false;
          if(input == stdin)
          {
            printf("ready>>> ");
          }
          return get_token();
        }
        else if(id == "import")
        {
          while(isspace(last)) // Check for whitespace (and ignore)
          {
            last = getc(input);
          }
          id = "";
          while(!(last == ';'))
          {
            id += last;
            last = getc(input);
          }
          file_stack.push_back(source);
          if(PRINT_DEBUG_FLAG)
          {
            printf("Preparing to read file: %s\n", id.c_str());
          }
          source = fopen(id.c_str(), "r");
          if(!source)
          {
            printf("ERROR READING FILE! Returning to parent file!\n");
            source = file_stack.back();
            file_stack.pop_back();
          }
          return static_cast<Token>(';');
        }
        return tok_id;
      }
      else if(isdigit(last) || last == '.') // Check for numbers
      {
        std::string num;
        do
        {
          num += last;
          last = getc(input);
        } while(isdigit(last) || last == '.');
        val = strtod(num.c_str(), 0);
        return tok_num;
      }
      else if(last == '#') // Check for comments
      {
        last = getc(input);
        if(last == '#') // Skip multiline comments
        {
          unsigned count = 0;
          while(count < 2)
          {
            last = getc(input);
            if(last == '#')
            {
              count++;
            }
            else if(last == EOF)
            {
              break;
            }
            else
            {
              count = 0;
            }
          }
        }
        else // Otherwise skip comment line
        {
          while(last != EOF && last != '\n' && last != '\r' && last != '#')
          {
            last = getc(input);
          }
        }
        // Check for EOF in comment
        if(last != EOF)
        {
          return get_token(input);
        }
      }
      // Check for EOF token
      if(last == EOF)
      {
        if(PRINT_DEBUG_FLAG)
        {
          printf("FILES LEFT IN STACK: %zu\n", file_stack.size());
        }
        if(!file_stack.empty())
        {
          if(PRINT_DEBUG_FLAG)
          {
            printf("Closing file...\n");
          }
          fclose(source);
          source = file_stack.back();
          file_stack.pop_back();
          last = ';';
          return static_cast<Token>(';');
        }
        return tok_eof;
      }
      // Otherwise return character
      int curr = last;
      last = getc(input);
      return static_cast<Token>(curr);
    }

    inline Token get_next()
    {
      return curr_tok = get_token();
    }

    inline Token get_curr()
    {
      return curr_tok;
    }

  };

}

#endif
