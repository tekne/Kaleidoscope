// --- main.cpp ---
// A simple program implementing a REPL for the Kaleidoscope language.
// Based off the LLVM Kaleidoscope tutorial found at
// https://llvm.org/docs/tutorial
//
// Uses version 5.0.1 of LLVM and Clang
//
// --- Copyright (C) Jad Elkhaleq Ghalayini, 2018 ---
// This file is distributed under the MIT License.
// See LICENSE.TXT for details

#include "codegen.hpp"

// Useful functions for the language

#ifdef LLVM_ON_WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

/// putchard - putchar that takes a double and returns 0.
extern "C" DLLEXPORT double putchard(double X) {
  fputc((char)X, stderr);
  return 0;
}

/// printd - printf that takes a double prints it as "%f\n", returning 0.
extern "C" DLLEXPORT double printd(double X) {
  fprintf(stderr, "%f\n", X);
  return 0;
}

// Initialize a REPL for Kaleidoscope

int main()
{
  fprintf(stderr, "TOKENS (DEF, EXTERN, ID, NUM, EOF): %d, %d, %d, %d, %d\n",
  kaleidoscope::tok_def,
  kaleidoscope::tok_extern,
  kaleidoscope::tok_id,
  kaleidoscope::tok_num,
  kaleidoscope::tok_eof);
  kaleidoscope::Parser p;
  p.set_binary_precedence(':', 1);
  p.set_binary_precedence('=', 2);
  p.set_binary_precedence('<', 10);
  p.set_binary_precedence('+', 20);
  p.set_binary_precedence('-', 30);
  p.set_binary_precedence('*', 40);
  p.run_parser();
}
