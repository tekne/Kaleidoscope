// --- codegen.hpp ----
// Code generation for a slightly modified version of the Kaleidoscope language.
// Based off the LLVM Kaleidoscope tutorial found at
// https://llvm.org/docs/tutorial
//
// Uses version 5.0.1 of LLVM and Clang
//
// --- Copyright (C) Jad Elkhaleq Ghalayini, 2018 ---
// This file is distributed under the MIT License.
// See LICENSE.TXT for details

#ifndef KALEIDOSCOPE_CODEGEN_INCLUDED
#define KALEIDOSCOPE_CODEGEN_INCLUDED

#include "llvm-5.0/llvm/ADT/APFloat.h"
#include "llvm-5.0/llvm/ADT/STLExtras.h"
#include "llvm-5.0/llvm/IR/BasicBlock.h"
#include "llvm-5.0/llvm/IR/Constants.h"
#include "llvm-5.0/llvm/IR/DerivedTypes.h"
#include "llvm-5.0/llvm/IR/Function.h"
#include "llvm-5.0/llvm/IR/IRBuilder.h"
#include "llvm-5.0/llvm/IR/LLVMContext.h"
#include "llvm-5.0/llvm/IR/LegacyPassManager.h"
#include "llvm-5.0/llvm/IR/Module.h"
#include "llvm-5.0/llvm/IR/Type.h"
#include "llvm-5.0/llvm/IR/Verifier.h"
#include "llvm-5.0/llvm/Support/TargetSelect.h"
#include "llvm-5.0/llvm/Target/TargetMachine.h"
#include "llvm-5.0/llvm/Transforms/Scalar.h"
#include "llvm-5.0/llvm/Transforms/Scalar/GVN.h"

#include <map>

#include "parser.hpp"

namespace kaleidoscope
{

  llvm::Value* val_error(const char* str)
  {
    fprintf(stderr, "VALUE ERROR: %s\n", str);
    return nullptr;
  }

  llvm::Value* val_error(std::string str)
  {
    return val_error(str.c_str());
  }

  llvm::Value* numeric_expr::codegen(generator& g)
  {
    return llvm::ConstantFP::get(g.context, llvm::APFloat(val));
  }

  llvm::Value* var_expr::codegen(generator& g)
  {
    llvm::Value* V = g.names[name];
    if(!V)
    {
      return val_error("Unknown variable name " + name);
    }
    return g.builder.CreateLoad(V, name.c_str());
  }

  llvm::Value* unary_expr::codegen(generator& g)
  {
    llvm::Value* operand_val = operand->codegen(g);
    if(!operand_val)
    {
      return nullptr;
    }

    llvm::Function* F = g.get_func(std::string("unary") + op);
    if(!F)
    {
      return val_error("Unknown unary operator");
    }

    return g.builder.CreateCall(F, operand_val, "unop");
  }

  llvm::Value* binary_expr::codegen(generator& g)
  {
    // Special case for the '=' operator:
    if(op == '=')
    {
      var_expr* lhse = dynamic_cast<var_expr*>(lhs.get());
      if(!lhse)
      {
        return val_error("Destination of '=' must be a variable!");
      }

      llvm::Value* val = rhs->codegen(g);
      if(!val)
      {
        return val_error("RHS generation failed!");
      }

      llvm::Value* var = g.names[lhse->get_name()];
      if(!var)
      {
        return val_error("Unknown variable name!");
      }

      g.builder.CreateStore(val, var);
      return val;
    }

    // Other operators
    llvm::Value *L = lhs->codegen(g), *R = rhs->codegen(g);
    if(!L && R)
    {
      return val_error("LHS generation failed!");
    }
    else if(L && !R)
    {
      return val_error("RHS generation failed!");
    }
    else if(!L || !R)
    {
      return val_error("Both LHS and RHS generation failed!");
    }
    else
    {
      switch(op)
      {
        case ':':
          return R;
        case '+':
          return g.builder.CreateFAdd(L, R, "addtmp");
        case '-':
          return g.builder.CreateFSub(L, R, "subtmp");
        case '*':
          return g.builder.CreateFMul(L, R, "multmp");
        case '<':
          L = g.builder.CreateFCmpULT(L, R, "cmptmp");
          return g.builder.CreateUIToFP(L, llvm::Type::getDoubleTy(g.context));
        default:
          break;
      }

      llvm::Function* F = g.get_func(std::string("binary") + op);
      if(!F)
      {
        return val_error(std::string("Operator") + op + "not found!");
      }

      llvm::Value* Ops[2] = {L, R};
      return g.builder.CreateCall(F, Ops, "binop");
    }
  }

  llvm::Value* call_expr::codegen(generator& g)
  {
    llvm::Function* called = g.get_func(name);
    llvm::Function* func = g.builder.GetInsertBlock()->getParent();
    llvm::AllocaInst* temp = nullptr;

    if(!called)
    {
      return val_error("Unknown function " + name + " referenced");
    }
    if(called->arg_size() != args.size())
    {
      return val_error("Expected " + std::to_string(called->arg_size())
      + " arguments, got " + std::to_string(args.size()));
    }

    std::vector<llvm::Value*> argsv;
    auto f_args = called->arg_begin();
    for(auto& arg: args)
    {
      if(!f_args->getType()->isPointerTy())
      {
        // Pass value
        argsv.push_back(arg->codegen(g));
        if(!argsv.back())
        {
          return nullptr;
        }
      }
      else if(auto vexpr = dynamic_cast<var_expr*>(arg.get()))
      {
        // Pass pointer to alloca'd variable
        argsv.push_back(g.names[vexpr->get_name()]);
      }
      else
      {
        // Create temporary alloca to store variable in
        temp = g.createEntryAllocaInst(func, "calltemp");
        // Store the value into the alloca
        g.builder.CreateStore(arg->codegen(g), temp);
        // Add the alloca to the list of function parameters
        argsv.push_back(temp);
      }
    }

    return g.builder.CreateCall(called, argsv, "calltmp");
  }

  llvm::Value* ternary_expr::codegen(generator& g)
  {
    llvm::Value* cond_val = cond->codegen(g);
    if(!cond_val)
    {
      return nullptr;
    }

    cond_val = g.builder.CreateFCmpONE(
      cond_val, llvm::ConstantFP::get(g.context, llvm::APFloat(0.0)), "ifcond"
    );

    llvm::Function* func = g.builder.GetInsertBlock()->getParent();

    llvm::BasicBlock* then_block =
      llvm::BasicBlock::Create(g.context, "then", func);
    llvm::BasicBlock* else_block =
      llvm::BasicBlock::Create(g.context, "else");
    llvm::BasicBlock* merge_block =
      llvm::BasicBlock::Create(g.context, "ifcont");

    g.builder.CreateCondBr(cond_val, then_block, else_block);

    // Emit then value
    g.builder.SetInsertPoint(then_block);

    llvm::Value* then_val = then->codegen(g);
    if(!then_val)
    {
      return val_error("then value generation failed");
    }

    g.builder.CreateBr(merge_block);
    then_block = g.builder.GetInsertBlock();

    // Emit else block
    func->getBasicBlockList().push_back(else_block);
    g.builder.SetInsertPoint(else_block);

    llvm::Value* else_val = other->codegen(g);
    if(!else_val)
    {
      return val_error("else value generation failed");
    }

    g.builder.CreateBr(merge_block);
    else_block = g.builder.GetInsertBlock();

    // Emit merge block
    func->getBasicBlockList().push_back(merge_block);
    g.builder.SetInsertPoint(merge_block);
    // Create PHI node
    llvm::PHINode *PN =
      g.builder.CreatePHI(llvm::Type::getDoubleTy(g.context), 2, "iftmp");
    // Add incoming blocks and associated values
    PN->addIncoming(then_val, then_block);
    PN->addIncoming(else_val, else_block);
    // Return the PHI node as our value
    if(!PN)
    {
      return val_error("PHI node generation error!");
    }
    return PN;
  }

  llvm::Value* for_expr::codegen(generator& g)
  {
    llvm::Function* func = g.builder.GetInsertBlock()->getParent();

    // Create an alloca for the induction variable in the entry block
    llvm::AllocaInst* alloca = g.createEntryAllocaInst(func, var);

    // Emit the start code first
    llvm::Value* start_val = start->codegen(g);
    if(!start)
    {
      return val_error("Start generation failed!");
    }

    // Store the value into the alloca
    g.builder.CreateStore(start_val, alloca);

    // Make a new basic block for the loop header, inserting after current block
    llvm::BasicBlock* loop_block =
      llvm::BasicBlock::Create(g.context, "loop", func);

    // Explicitly fall through from current block to loop block
    g.builder.CreateBr(loop_block);

    // Insert into loop block
    g.builder.SetInsertPoint(loop_block);

    // We create the induction variable, and cache any shadowed variable
    llvm::Value* old_val = g.names[var];
    g.names[var] = alloca;
    if(!(g.names[var]))
    {
      return val_error("Induction variable not generated!");
    }

    // Emit the body of the loop
    if(!body->codegen(g))
    {
      return val_error("Body generation failed!");
    }

    if(!(g.names[var]))
    {
      return val_error("Name table corrupted after body generation!");
    }

    // We deal with the step value here
    llvm::Value* step_val = nullptr;
    if(step)
    {
      step_val = step->codegen(g);
      if(!step_val)
      {
        return val_error("Step generation failed!");
      }
    }
    else
    {
      step_val = llvm::ConstantFP::get(g.context, llvm::APFloat(1.0));
    }

    // We compute the end condition
    llvm::Value* end_cond = end->codegen(g);

    if(!end_cond)
    {
      return val_error("End generation failed!");
    }

    // Reload, increment and restore the alloca, allowing the body of the loop
    // to mutate the variable
    llvm::Value* curr_var = g.builder.CreateLoad(alloca, var.c_str());
    llvm::Value* next_var = g.builder.CreateFAdd(curr_var, step_val, "nextvar");
    g.builder.CreateStore(next_var, alloca);

    // We convert the end condition to a boolean by checking equality to 0
    end_cond = g.builder.CreateFCmpONE(
      end_cond, llvm::ConstantFP::get(g.context, llvm::APFloat(0.0)), "loopcond"
    );

    // Create the after loop block and insert it
    llvm::BasicBlock* after_block =
      llvm::BasicBlock::Create(g.context, "afterloop", func);

    // Insert conditional branch to end of loop_end_block
    g.builder.CreateCondBr(end_cond, loop_block, after_block);

    // Any new code goes into after_block
    g.builder.SetInsertPoint(after_block);

    // Restore shadowed values
    if(old_val)
    {
      g.names[var] = old_val;
    }
    else
    {
      g.names.erase(var);
    }

    // Always return 0 for now
    return llvm::Constant::getNullValue(llvm::Type::getDoubleTy(g.context));
  }

  llvm::Value* stack_expr::codegen(generator& g)
  {
    std::vector<llvm::Value*> old_bindings;
    llvm::Function* func = g.builder.GetInsertBlock()->getParent();

    // Register all variables and emit their initializer
    for(auto& var: var_names)
    {
      const std::string& name = var.first.get_name();
      expr* init = var.second.get();
      llvm::Value* init_val;
      if(init)
      {
        init_val = init->codegen(g);
        if(!init_val)
        {
          return val_error("Error generating code for var initializer value!");
        }
      }
      else // Use 0.0 as a default value
      {
        init_val = llvm::ConstantFP::get(g.context, llvm::APFloat(0.0));
      }

      llvm::AllocaInst* alloca = g.createEntryAllocaInst(func, name);
      g.builder.CreateStore(init_val, alloca);

      // Remember the old binding so we can restore it when unrecursing
      old_bindings.push_back(g.names[name]);
      g.names[name] = alloca;
    }
    // Codegen the body with all variables in scope
    llvm::Value* body_val = body->codegen(g);
    if(!body_val)
    {
      return val_error("Error generating code for body in stack expression!");
    }
    // Pop all variables from scope
    for(unsigned int i = 0, e = var_names.size(); i < e; i++)
    {
      g.names[var_names[i].first.get_name()] = old_bindings[i];
    }

    // Return the result of the body computation
    return body_val;
  }

  llvm::Function* prototype_expr::codegen(generator& g)
  {
    std::vector<llvm::Type*> types;
    for(const auto& x: args)
    {
      if(x.is_ref())
      {
        types.push_back(llvm::Type::getDoublePtrTy(g.context));
      }
      else
      {
        types.push_back(llvm::Type::getDoubleTy(g.context));
      }
    }

    llvm::FunctionType* FT = llvm::FunctionType::get(
      llvm::Type::getDoubleTy(g.context), types, false);
    llvm::Function* F = llvm::Function::Create(
      FT, llvm::Function::ExternalLinkage, name, g.module.get()
    );
    {
      unsigned idx = 0;
      for(auto& arg: F -> args())
      {
        arg.setName(args[idx++].get_name());
      }
    }
    return F;
  }

  llvm::Function* func_expr::codegen(generator& g)
  {
    // Check for prototype error
    if(!proto)
    {
      return
        static_cast<llvm::Function*>(val_error("Expected prototype, got NULL!"));
    }
    // Transfer ownership of the prototype to the prototypes map but keep
    // a reference of it for use below
    auto& P = *proto;
    g.prototypes[proto->get_name()] = std::move(proto);
    llvm::Function* func = g.get_func(P.get_name());
    if(!func)
    {
      func = proto->codegen(g);
    }

    if(!func)
    {
      return
        static_cast<llvm::Function*>(val_error("Expected function, got NULL!"));
    }

    // Create a new basic block to start insertion into
    llvm::BasicBlock* bb = llvm::BasicBlock::Create(g.context, "entry", func);
    g.builder.SetInsertPoint(bb);

    // Record function arguments in the names map
    g.names.clear();
    for(auto &arg: func->args())
    {
      if(!arg.getType()->isPointerTy())
      {
        // Create an alloca for this variable
        llvm::AllocaInst* alloca = g.createEntryAllocaInst(func, arg.getName());

        // Store the initial value into the alloca
        g.builder.CreateStore(&arg, alloca);

        // Add argument to the symbol table
        g.names[arg.getName()] = alloca;
      }
      else
      {
        g.names[arg.getName()] = &arg;
      }
    }

    if(llvm::Value* return_val = body->codegen(g))
    {
      // Finish off the function
      g.builder.CreateRet(return_val);

      // Validate the code for consistency
      llvm::verifyFunction(*func);

      // Optimize if the flag is enabled
      if(OPTIMIZATION_FLAG)
      {
        g.fpm->run(*func);
      }
      else
      {
        printf("WARNING: NOT OPTIMIZED!\n");
      }

      return func;
    }

    // Error reading body, erase from parent
    func->eraseFromParent();
    return static_cast<llvm::Function*>(val_error("Error reading body!"));
  }
}

#endif
