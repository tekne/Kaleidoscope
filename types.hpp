// --- types.hpp ---
// The type system for the Kaleidoscope language, supporting
// vectors and tensors
//
// No dependencies except the C++14 STL
//
// --- Copyright (C) 2018 Jad Elkhaleq Ghalayini ---
// This file is distributed under the MIT License.
// See LICENSE.TXT for details

#ifndef KALEIDOSCOPE_TYPES_INCLUDED
#define KALEIDOSCOPE_TYPES_INCLUDED

#include <cstddef>

namespace kaleidoscope
{

  class expression_type
  {
    bool is_reference;
    bool is_vector;
    std::size_t var_sizes;
    std::vector<std::size_t> sizes;
    bool error;
  public:
    expression_type(bool r = false, bool v = false, std::size_t vs = 0):
    is_reference(r), is_vector(v), var_sizes(vs), sizes(), error(false) {}
    expression_type(bool r, bool v, std::size_t vs, std::vector<std::size_t> s,
    bool e = false):
    is_reference(r), is_vector(v), var_sizes(vs), sizes(s), error(e) {};

    bool is_ref() const
    {
      return is_reference;
    }

    bool is_vec() const
    {
      return is_vector;
    }

    std::size_t rank() const
    {
      return sizes.size() + var_sizes;
    }

    std::size_t variable_sizes() const
    {
      return var_sizes;
    }

    const std::vector<std::size_t>& get_sizes() const
    {
      return sizes;
    }

    bool fixed_size() const
    {
      return !var_sizes;
    }

    bool well_formed() const
    {
      return !error
      && !(is_vec() && (variable_sizes() || get_sizes().size() != 1))
      && std::all_of(get_sizes().begin(),
        get_sizes().end(),
        [](std::size_t x){return x != 0;});
    }

    expression_type operator+(expression_type T)
    {
      return expression_type(
        false,
        T.is_vec() || is_vec(),
        T.variable_sizes(),
        T.get_sizes(),
        (T.variable_sizes() == variable_sizes()) &&
        (T.get_sizes() == get_sizes())
      );
    }

    expression_type operator-(expression_type T)
    {
      return operator+(T);
    }
  };

  class argument
  {
    std::string name;
    expression_type type;

  public:

    argument() {}

    argument(std::string nm,
      bool ref = false,
      bool vec = false,
      std::size_t vs = 0
    ): name(nm), type(ref, vec, vs){}

    argument(std::string nm,
      bool ref,
      bool vec,
      std::size_t vs,
      std::vector<std::size_t> sz
    ): name(nm), type(ref, vec, vs, sz) {}

    bool well_formed() const
    {
      return (name != "") && type.well_formed();
    }

    bool is_ref() const
    {
      return type.is_ref();
    }

    bool is_vec() const
    {
      return type.is_vec();
    }

    std::size_t rank() const
    {
      return type.rank();
    }

    std::size_t variable_sizes() const
    {
      return type.variable_sizes();
    }

    bool fixed_size() const
    {
      return type.fixed_size();
    }

    const std::vector<std::size_t>& get_sizes() const
    {
      return type.get_sizes();
    }

    const std::string& get_name() const
    {
      return name;
    }

    const char* c_str() const
    {
      return name.c_str();
    }

    void prettyprint(FILE* f) const
    {
      if(is_ref())
      {
        fprintf(f, "ref ");
      }
      if(is_vec())
      {
        fprintf(f, "vec ");
      }
      fprintf(f, "%s", c_str());
      for(auto x: get_sizes())
      {
        fprintf(f, "[%lu]", x);
      }
    }

  };

}

#endif
