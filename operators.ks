##
A program to add additional operators to a slightly modified version of the
Kaleidoscope language. Adapted from the Kaleidoscope tutorial at
https://llvm.org/docs/tutorial/

This file is a lightly edited version of an example program found in the
Kaleidoscope tutorial, which is licensed under the University of Illinois Open
Source License.

This file is licensed under the MIT License. See LICENSE.TXT for details.
##

# Logical unary not.
def unary!(v)
  if v then
    0
  else
    1;

# Unary negate.
def unary-(v)
  0 - v;

# Define > with the same precedence as <.
def binary> 10 (LHS, RHS)
  RHS < LHS;

# Binary logical or, which does not short circuit.
def binary| 5 (LHS, RHS)
  if LHS then
    1
  else if RHS then
    1
  else
    0;

# Binary logical and, which does not short circuit.
def binary& 6 (LHS, RHS)
  if !LHS then
    0
  else
    !!RHS;

# Define ~ as equality with slightly lower precedence than relationals.
def binary~ 9 (LHS, RHS)
  !(LHS < RHS | LHS > RHS);
