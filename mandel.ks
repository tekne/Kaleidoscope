##
A program to display the Mandelbrot set written in a slightly modified
version of the Kaleidoscope language. Adapted from the Kaleidoscope tutorial at
https://llvm.org/docs/tutorial/

This file is a lightly edited version of an example program found in the
Kaleidoscope tutorial, which is licensed under the University of Illinois Open
Source License.

This file is licensed under the MIT License. See LICENSE.TXT for details.
##

# Import the operators used in the program
import operators.ks;

extern putchard(char);

def printdensity(d)
  if d > 8 then
    putchard(32)  # ' '
  else if d > 4 then
    putchard(46)  # '.'
  else if d > 2 then
    putchard(43)  # '+'
  else
    putchard(42); # '*'

# Determine whether the specific location diverges.
# Solve for z = z^2 + c in the complex plane.
def mandelconverger(real, imag, iters, creal, cimag)
  if iters > 255 | (real*real + imag*imag > 4) then
    iters
  else
    mandelconverger(real*real - imag*imag + creal,
                    2*real*imag + cimag,
                    iters+1, creal, cimag);

# Return the number of iterations required for the iteration to escape
def mandelconverge(real, imag)
  mandelconverger(real, imag, 0, real, imag);

# Compute and plot the mandelbrot set with the specified 2 dimensional range
# info.

def mandelline(xmin, xmax, xstep, y)
  for x = xmin, x < xmax, xstep do
   printdensity(mandelconverge(x,y));

def mandelhelp(xmin, xmax, xstep, ymin, ymax, ystep)
    for y = ymin, y < ymax, ystep do (
      mandelline(xmin, xmax, xstep, y) : putchard(10)
    );

# mandel - This is a convenient helper function for plotting the mandelbrot set
# from the specified position with the specified Magnification.
def mandel(realstart, imagstart, realmag, imagmag)
    mandelhelp(realstart, realstart+realmag*78, realmag,
               imagstart, imagstart+imagmag*40, imagmag);
