// --- parser.hpp ----
// A simple parser for a slightly modified version of the Kaleidoscope language.
// Based off the LLVM Kaleidoscope tutorial found at
// https://llvm.org/docs/tutorial
//
// Uses version 5.0.1 of LLVM and Clang
//
// --- Copyright (C) Jad Elkhaleq Ghalayini, 2018 ---
// This file is distributed under the MIT License.
// See LICENSE.TXT for details

#ifndef KALEIDOSCOPE_PARSE_INCLUDED
#define KALEIDOSCOPE_PARSE_INCLUDED

#include <string>
#include <vector>
#include <cstdio>
#include <map>
#include <memory>
#include <cassert>
#include <algorithm>

#include "llvm-5.0/llvm/ADT/APFloat.h"
#include "llvm-5.0/llvm/ADT/STLExtras.h"
#include "llvm-5.0/llvm/IR/BasicBlock.h"
#include "llvm-5.0/llvm/IR/Constants.h"
#include "llvm-5.0/llvm/IR/DerivedTypes.h"
#include "llvm-5.0/llvm/IR/Function.h"
#include "llvm-5.0/llvm/IR/IRBuilder.h"
#include "llvm-5.0/llvm/IR/LLVMContext.h"
#include "llvm-5.0/llvm/IR/LegacyPassManager.h"
#include "llvm-5.0/llvm/IR/Module.h"
#include "llvm-5.0/llvm/IR/Type.h"
#include "llvm-5.0/llvm/IR/Verifier.h"
#include "llvm-5.0/llvm/Support/TargetSelect.h"
#include "llvm-5.0/llvm/Target/TargetMachine.h"
#include "llvm-5.0/llvm/Transforms/Scalar.h"
#include "llvm-5.0/llvm/Transforms/Scalar/GVN.h"
#include "Kaleidoscope.h"

#include "lexer.hpp"
#include "types.hpp"

namespace kaleidoscope
{

  class generator;

  class expr
  {
  public:
    virtual ~expr() {}
    virtual void prettyprint(FILE* f = stderr) {}
    virtual llvm::Value* codegen(generator&) = 0;
  };

  class numeric_expr: public expr
  {
    double val;

  public:
    numeric_expr(double v): val(v) {}
    void prettyprint(FILE* f = stderr) {fprintf(f, "%lf", val);}
    llvm::Value* codegen(generator&);
  };

  class array_expr: public expr
  {
    std::vector<std::unique_ptr<expr>> elements;

  public:
    array_expr(std::vector<std::unique_ptr<expr>> elems):
    elements(std::move(elems)) {}
    void prettyprint(FILE* f = stderr)
    {
      fprintf(f, "{");
      if(elements.empty())
      {
        fprintf(f, "}");
        return;
      }
      elements[0]->prettyprint(f);
      for(std::size_t i = 1; i < elements.size(); i++)
      {
        fprintf(f, ",");
        elements[i]->prettyprint(f);
      }
      fprintf(f, "}");
    }
  };

  class var_expr: public expr
  {
    std::string name;
    std::vector<std::size_t> sizes;

  public:
    var_expr(std::string n): name(n) {}
    void prettyprint(FILE* f = stderr)
    {
      fprintf(f, "%s", name.c_str());
      for(auto x: sizes)
      {
        fprintf(f, "[");
        if(x)
        {
          fprintf(f, "%lu", x);
        }
        fprintf(f, "]");
      }
    }
    llvm::Value* codegen(generator&);
    const std::string& get_name() const {return name;}
  };

  class unary_expr: public expr
  {
    char op;
    std::unique_ptr<expr> operand;

  public:
    unary_expr(char oc, std::unique_ptr<expr> opr):
    op(oc), operand(std::move(opr)) {}

    llvm::Value* codegen(generator& g);
  };


  class binary_expr: public expr
  {
    char op;
    std::unique_ptr<expr> lhs, rhs;

  public:
    binary_expr(char opr,
      std::unique_ptr<expr> left, std::unique_ptr<expr> right):
      op(opr), lhs(std::move(left)), rhs(std::move(right)) {}
    void prettyprint(FILE* f = stderr)
    {
      fprintf(f, "(");
      lhs->prettyprint(f);
      fprintf(f, " %c ", op);
      rhs->prettyprint(f);
      fprintf(f, ")");
    }
    llvm::Value* codegen(generator&);
  };

  class ternary_expr: public expr
  {
    std::unique_ptr<expr> cond, then, other;
  public:
    ternary_expr(std::unique_ptr<expr> c,
      std::unique_ptr<expr> l, std::unique_ptr<expr> r):
      cond(std::move(c)), then(std::move(l)), other(std::move(r)) {}
    llvm::Value* codegen(generator&);
    void prettyprint(FILE* f = stderr)
    {
      fprintf(f, "if ");
      cond->prettyprint(f);
      fprintf(f, "\nthen ");
      then->prettyprint(f);
      fprintf(f, "\nelse ");
      other->prettyprint(f);
      fprintf(f, "\n");
    }
  };

  class for_expr: public expr
  {
    std::string var;
    std::unique_ptr<expr> start, end, step, body;

  public:

    for_expr(std::string name,
    std::unique_ptr<expr> s, std::unique_ptr<expr> e,
    std::unique_ptr<expr> st, std::unique_ptr<expr> b
    ): var(name), start(std::move(s)), end(std::move(e)), step(std::move(st)),
    body(std::move(b))
    {
      //printf("Generated a for expression with induction variable %s\n", name.c_str());
    }

    llvm::Value* codegen(generator&);

    void prettyprint(FILE* f = stderr)
    {
      fprintf(f, "for ");
      start->prettyprint(f);
      fprintf(f, ", ");
      end->prettyprint(f);
      if(step)
      {
        fprintf(f, ", ");
        step->prettyprint(f);
      }
      fprintf(f, " in\n");
      body->prettyprint(f);
      fprintf(f, "\n");
    }
  };

  class call_expr: public expr
  {
    std::string name;
    std::vector<std::unique_ptr<expr>> args;

  public:
    call_expr(std::string n, std::vector<std::unique_ptr<expr>> a):
    name(n), args(std::move(a)) {}
    void prettyprint(FILE* f = stderr)
    {
      fprintf(f, "%s(", name.c_str());
      for(std::size_t i = 0; i < args.size() - 1; i++)
      {
        args[i]->prettyprint(f);
        fprintf(f, ", ");
      }
      if(args.size())
      {
        args.back()->prettyprint(f);
      }
      fprintf(f, ")");
    }
    const std::string& get_name() const {return name;}
    llvm::Value* codegen(generator&);
  };

  class subscript_expr: public call_expr
  {
  public:
    subscript_expr(std::string n, std::vector<std::unique_ptr<expr>> a):
    call_expr(n, std::move(a)) {}
  };

  class prototype_expr
  {
    std::string name;
    std::vector<argument> args;
    bool is_op;
    unsigned precedence;

  public:
    prototype_expr(std::string n,
      std::vector<argument> a,
      bool isop = false,
      unsigned prec = 30)
    : name(n), args(a), is_op(isop), precedence(prec) {}
    virtual void prettyprint(FILE* f = stderr)
    {
      fprintf(stderr, "DEFINITION (%zu): %s(", args.size(), name.c_str());
      if(args.size())
      {
        for(auto i = args.begin(); i < args.end() - 1; i++)
        {
          i->prettyprint(f);
          fprintf(f, ", ");
        }
        args.back().prettyprint(f);
      }
      fprintf(f, ")\n");
    }
    const std::string& get_name() const {return name;}
    llvm::Function* codegen(generator&);

    bool is_unary() const {return args.size() == 1;}
    bool is_binary() const {return args.size() == 2;}
    bool is_unary_op() const {return is_op && is_unary();}
    bool is_binary_op() const {return is_op && is_binary();}

    char get_op_name() const
    {
      assert(is_unary_op() || is_binary_op());
      return name.back();
    }

    unsigned get_binary_precedence() const
    {
      return precedence;
    }
  };

  class func_expr
  {
    std::unique_ptr<prototype_expr> proto;
    std::unique_ptr<expr> body;

  public:
    func_expr(std::unique_ptr<prototype_expr> p, std::unique_ptr<expr> b):
    proto(std::move(p)), body(std::move(b)) {
    }
    virtual void prettyprint(FILE* f = stderr)
    {
      proto->prettyprint(f);
      fprintf(f, "BODY: ");
      body->prettyprint(f);
      fprintf(f, "\n");
    }
    llvm::Function* codegen(generator&);
  };

  class stack_expr: public expr
  {
    std::vector<std::pair<argument, std::unique_ptr<expr>>> var_names;
    std::unique_ptr<expr> body;

  public:
    stack_expr(
      std::vector<std::pair<argument, std::unique_ptr<expr>>> vn,
      std::unique_ptr<expr> bd
    ): var_names(std::move(vn)), body(std::move(bd)) {}

    llvm::Value* codegen(generator& g);
  };

  std::unique_ptr<expr> parse_error(const char* str, int code = 'U')
  {
    fprintf(stderr, "PARSE ERROR (%d): %s\n", code, str);
    return nullptr;
  }

  std::unique_ptr<expr> parse_error(std::string str, int code = 'U')
  {
    parse_error(str.c_str(), code);
    return nullptr;
  }

  std::unique_ptr<prototype_expr> parse_error_p(const char* str, int code = tok_err)
  {
    fprintf(stderr, "PROTOTYPE PARSE ERROR (%d): %s\n", code, str);
    return nullptr;
  }

  std::unique_ptr<prototype_expr> parse_error_p(std::string str, int code= tok_err)
  {
    parse_error_p(str.c_str(), code);
    return nullptr;
  }

  argument bad_argument(const char* str)
  {
    fprintf(stderr, "BAD ARGUMENT: %s\n", str);
    return argument();
  }

  argument bad_argument(std::string str)
  {
    return bad_argument(str.c_str());
  }

  class generator
  {
  public:
    llvm::LLVMContext context;
    llvm::IRBuilder<> builder;
    std::map<std::string, llvm::Value*> names;
    std::map<std::string, std::unique_ptr<prototype_expr>> prototypes;

    // Owned objects on the heap
    std::unique_ptr<llvm::Module> module;
    std::unique_ptr<llvm::legacy::FunctionPassManager> fpm;
    std::unique_ptr<llvm::orc::KaleidoscopeJIT> jit;

    void init_mod()
    {
      // Open a new module
      module = llvm::make_unique<llvm::Module>("My Cool JIT", context);
      module->setDataLayout(jit->getTargetMachine().createDataLayout());

      // Create a new pass manager
      fpm = llvm::make_unique<llvm::legacy::FunctionPassManager>(module.get());

      // Add optimizer passes
      // Promote allocas to registers
      fpm->add(llvm::createPromoteMemoryToRegisterPass());
      // Twiddle bits, combine instructions
      fpm->add(llvm::createInstructionCombiningPass());
      // Reassociate
      fpm->add(llvm::createReassociatePass());
      // Eliminate common subexpressions
      fpm->add(llvm::createGVNPass());
      // Simplify control flow graph
      fpm->add(llvm::createCFGSimplificationPass());

      fpm->doInitialization();
    }

    llvm::Function* get_func(std::string name)
    {
      if(module)
      {
        if(auto *F = module->getFunction(name))
        {
          return F;
        }
        else
        {
          auto FI = prototypes.find(name);
          if(FI != prototypes.end())
          {
            return FI->second->codegen(*this);
          }
        }
      }
      return nullptr;
    }

    inline auto jit_module()
    {
      auto H = jit->addModule(std::move(module));
      init_mod();
      return H;
    }

    llvm::AllocaInst* createEntryAllocaInst(llvm::Function* F,
      const std::string& name)
      {
        llvm::IRBuilder<> temporary(&F->getEntryBlock(),
                                    F->getEntryBlock().begin());
        return temporary.CreateAlloca(llvm::Type::getDoubleTy(context), 0,
                                      name.c_str());
      }

    generator():
    builder(context),
    fpm(llvm::make_unique<llvm::legacy::FunctionPassManager>(module.get()))
    {
      // Prepare the target for the JIT compiler
      llvm::InitializeNativeTarget();
      llvm::InitializeNativeTargetAsmPrinter();
      llvm::InitializeNativeTargetAsmParser();

      // Initialize the JIT compiler
      jit = llvm::make_unique<llvm::orc::KaleidoscopeJIT>();

      // Initialize the module
      init_mod();
    }

  };

  class Parser: public Lexer
  {
    generator g;
  public:
    std::unique_ptr<expr> parse_expr();

    argument get_arg()
    {
      bool is_ref = false;
      bool is_vec = false;
      while(get_next() != tok_id)
      {
        switch(get_curr())
        {
        case tok_ref:
          if(is_ref) return bad_argument("Cannot have argument qualified as 'ref' twice!");
          is_ref = true;
          break;
        case tok_vec:
          if(is_vec) return bad_argument("Cannot have argument qualified as 'vec' twice!");
          is_vec = true;
          break;
        default:
          return bad_argument("Invalid token " + std::to_string(get_curr()) + " when parsing argument!");
        }
      }
      auto id = get_id();
      std::size_t var_sizes = 0;
      std::vector<std::size_t> sizes;
      while(get_next() == '[')
      {
        if(get_next() == tok_num)
        {
          sizes.push_back(get_val());
          if(get_next() != ']')
          {
            return bad_argument("Expected ']' after subscript value!");
          }
        }
        else if(get_curr() == ']')
        {
          if(is_vec)
          {
            return bad_argument("Vectors cannot have a variable size!");
          }
          var_sizes++;
        }
        else
        {
          return bad_argument("Expected numeric value or ']' in array size!");
        }
      }
      //TODO: reconsider
      if(is_vec && (sizes.size() != 1))
      {
        return bad_argument("Vectors must be exactly rank-1!");
      }
      return argument(id, is_ref, is_vec, var_sizes, sizes);
    }
  private:

    std::map<char, int> binary_precedence;

    std::unique_ptr<expr> parse_number_expr()
    {
      auto result = llvm::make_unique<numeric_expr>(get_val());
      get_next(); // Eat the number
      return std::move(result);
    }

    std::unique_ptr<expr> parse_paren_expr()
    {
      get_next(); // Eat '('
      auto result = parse_expr();
      if(!result)
      {
        return nullptr;
      }
      if(get_curr() != ')')
      {
        return parse_error("Expected ')'", get_curr());
      }
      get_next(); // Eat ')'
      return result;
    }

    std::unique_ptr<expr> parse_id_expr()
    {
      std::string id = get_id();
      get_next(); // Eat id

      if(get_curr() != '(') // Not a function call
      {
        return llvm::make_unique<var_expr>(id);
      }
      else // A function call
      {
        get_next(); // Eat '('
        std::vector<std::unique_ptr<expr>> args;
        if(get_curr() != ')')
        {
          while(true)
          {
            if(auto arg = parse_expr())
            {
              args.push_back(std::move(arg));
            }
            else
            {
              return nullptr;
            }

            if(get_curr() == ')')
            {
              break;
            }
            if(get_curr() != ',')
            {
              return parse_error("Expected ')' or ',' in argument list", get_curr());
            }
            get_next();
          }

          get_next(); // Eat ')'
        }
        return llvm::make_unique<call_expr>(id, std::move(args));
      }
    }

    std::unique_ptr<expr> parse_ternary_expr()
    {
      get_next(); // Eat the 'if'

      // Get the condition
      auto cond = parse_expr();
      if(!cond) // Return error on invalid condition expression
      {
        return nullptr;
      }

      if(get_curr() != tok_then)
      {
        return parse_error("Expected then");
      }
      else
      {
        get_next(); // Eat the 'then'
      }

      // Get the left expression
      auto then = parse_expr();
      if(!then) // Return error on invalid condition expression
      {
        return nullptr;
      }

      if(get_curr() != tok_else)
      {
        return parse_error("Expected else");
      }
      else
      {
        get_next(); // Eat the 'then'
      }

      // Get the right expression
      auto other = parse_expr();
      if(!other)
      {
        return nullptr;
      }

      return llvm::make_unique<ternary_expr>
      (std::move(cond), std::move(then), std::move(other));
    }

    std::unique_ptr<expr> parse_for_expr()
    {
      get_next(); // Eat 'for'

      if(get_curr() != tok_id)
      {
        return parse_error("Expected identifier after 'for'");
      }
      std::string id = get_id();
      get_next(); // Eat id
      if(get_curr() != '=')
      {
        return parse_error("Expected '=' after loop variable");
      }
      get_next(); // Eat '='

      auto start = parse_expr();
      if(!start)
      {
        return nullptr;
      }

      if(get_curr() != ',')
      {
        return parse_error("Expected ',' after loop variable");
      }
      get_next(); // Eat ','
      auto end = parse_expr();
      if(!end)
      {
        return nullptr;
      }

      std::unique_ptr<expr> step;
      if(get_curr() == ',') // Step is optional
      {
        get_next(); // Eat ',' if present
        step = parse_expr();
        if(!step)
        {
          return nullptr;
        }
      }

      if(get_curr() != tok_do)
      {
        return parse_error("Expected ',' or 'in' after end value");
      }
      get_next(); // Eat 'in'

      auto body = parse_expr();
      if(!body)
      {
        return nullptr;
      }

      return llvm::make_unique<for_expr>(id,
        std::move(start), std::move(end),std::move(step), std::move(body)
      );
    }

    std::unique_ptr<expr> parse_stack_expr()
    {
      std::vector<std::pair<argument, std::unique_ptr<expr>>> var_names;
      argument arg;

      while(true)
      {
        arg = get_arg();
        if(!arg.well_formed())
        {
          return parse_error("Argument to 'let' not well-formed!");
        }
        else if(arg.is_ref())
        {
          return parse_error("Variable cannot be a reference!");
        }

        std::unique_ptr<expr> init;
        if(get_curr() == '=')
        {
          get_next(); // Eat the '='
          init = parse_expr();
          if(!init)
          {
            return parse_error("Error parsing initializer!");
          }
        }
        var_names.push_back(std::make_pair(arg, std::move(init)));
        if(get_curr() != ',')
        {
          break;
        }
      }

      if(get_curr() != tok_then)
      {
        return parse_error("Expected keyword 'then' after 'let'", get_curr());
      }
      get_next(); // Eat 'in'

      auto body = parse_expr();
      if(!body)
      {
        return parse_error("Error parsing body!");
      }

      return llvm::make_unique<stack_expr>(std::move(var_names), std::move(body));
    }

    std::unique_ptr<expr> parse_primary()
    {
      //fprintf(stderr, "Parsing a primary expression (%c,%d)\n", get_curr(), get_curr());
      switch(get_curr())
      {
        default:
          return parse_error("Unknown token when expecting an expression", get_curr());
        case tok_id:
          return parse_id_expr();
        case tok_num:
          return parse_number_expr();
        case tok_if:
          return parse_ternary_expr();
        case tok_for:
          return parse_for_expr();
        case tok_let:
          return parse_stack_expr();
        case '(':
          return parse_paren_expr();
      }
    }

    int get_prec()
    {
      auto it = binary_precedence.find(get_curr());
      if(it == binary_precedence.end())
      {
        return -1;
      }
      else
      {
        return (*it).second;
      }
    }

    std::unique_ptr<expr> parse_unary()
    {
      if(!isascii(get_curr()) || get_curr() == '(' || get_curr() == ',')
      {
        return parse_primary();
      }
      int opcode = get_curr();
      get_next();
      if(auto operand = parse_unary())
      {
        return llvm::make_unique<unary_expr>(opcode, std::move(operand));
      }
      return nullptr;
    }

    std::unique_ptr<expr> parse_bin_rhs(int exprp,
      std::unique_ptr<expr> lhs)
    {
      while(true)
      {
        int tokp = get_prec();
        if(tokp < exprp)
        {
          return lhs;
        }
        else
        {
          int binop = get_curr();
          get_next(); // Eat binary operation
          auto rhs = parse_unary();
          if(!rhs)
          {
            return nullptr;
          }
          int nxtp = get_prec();
          if(tokp < nxtp)
          {
            rhs = parse_bin_rhs(tokp + 1, std::move(rhs));
            if(!rhs)
            {
              return nullptr;
            }
          }
          lhs = llvm::make_unique<binary_expr>(binop,
            std::move(lhs), std::move(rhs));
        }
      }
    }

    std::unique_ptr<prototype_expr> parse_proto()
    {
      unsigned kind = 0;
      unsigned precedence = 30;
      std::string FnName;

      switch(get_curr())
      {
      default:
        return parse_error_p("Expected function name in prototype");
      case tok_id:
        FnName = get_id();
        get_next();
        break;
      case tok_unary:
        get_next();
        if(!isascii(get_curr()))
        {
          return parse_error_p("Expected unary operator");
        }
        FnName = "unary";
        FnName += static_cast<char>(get_curr());
        kind = 1;
        get_next();
        break;
      case tok_binary:
        get_next();
        if(!isascii(get_curr()))
        {
          return parse_error_p("Expected binary operator!");
        }
        FnName = "binary";
        FnName += get_curr();
        kind = 2;
        get_next();
        if(get_curr() == tok_num)
        {
          if(get_val() < 1 || get_val() > 100)
          {
            return parse_error_p("Invalid precedence, must be 1..100");
          }
          precedence = static_cast<unsigned>(get_val());
          get_next();
        }
        break;
      }


      if (get_curr() != '(')
        return parse_error_p("Expected '(' in prototype", get_curr());

        std::vector<argument> arguments;
        while (true)
        {
          arguments.push_back(get_arg());
          if(!arguments.back().well_formed())
          {
            return parse_error_p("Function argument not well formed!");
          }
          if(get_curr() != ',')
          {
            break;
          }
        }
        if (get_curr() != ')')
          return parse_error_p("Expected ')' in prototype", get_curr());

        // success.
        get_next(); // eat ')'.
        //fprintf(stderr, "Parsed prototype (%c,%d)\n", get_curr(), get_curr());

        return llvm::make_unique<prototype_expr>(FnName, std::move(arguments),
        kind!=0, precedence);
  }

    std::unique_ptr<func_expr> parse_def()
    {
      get_next(); // Eat "def"
      auto proto = parse_proto();
      if(!proto)
      {
        return nullptr;
      }
      if(auto E = parse_expr())
      {
        if(proto->is_binary_op())
        {
          binary_precedence[proto->get_op_name()] = proto->get_binary_precedence();
        }
        return llvm::make_unique<func_expr>(std::move(proto), std::move(E));
      }
      return nullptr;
    }

    std::unique_ptr<prototype_expr> parse_extern()
    {
      get_next(); // Eat `extern`
      return parse_proto();
    }

    std::unique_ptr<func_expr> parse_top()
    {
      if(auto E = parse_expr())
      {
        auto P = llvm::make_unique<prototype_expr>("__anon_expr", std::vector<argument>());
        return llvm::make_unique<func_expr>(std::move(P), std::move(E));
      }
      return nullptr;
    }

    void handle_def()
    {
      if(auto p = parse_def())
      {
        if(PRINT_DEBUG_FLAG)
        {
          fprintf(stderr, "Parsed a function definition:\n\n");
          p -> prettyprint(); fprintf(stderr, "\n");
        }
        if(auto * ir = p->codegen(g))
        {
          if(PRINT_IR_FLAG)
          {
            fprintf(stderr, "Intermediate Representation:\n");
            ir->print(llvm::errs());
            fprintf(stderr, "\n");
          }
          g.jit_module();
        }
        else
        {
          fprintf(stderr, "IR generation failed!\n");
        }
      }
      else
      {
        get_next();
      }
    }

    void handle_extern()
    {
      if(auto p = parse_extern())
      {
        if(PRINT_DEBUG_FLAG)
        {
          fprintf(stderr, "Parsed an extern function:\n\n");
          p -> prettyprint(); fprintf(stderr, "\n");
        }
        if(auto * ir = p->codegen(g))
        {
          if(PRINT_IR_FLAG)
          {
            fprintf(stderr, "Intermediate Representation:\n");
            ir->print(llvm::errs());
            fprintf(stderr, "\n");
          }
          g.prototypes[p->get_name()] = std::move(p);
        }
        else
        {
          fprintf(stderr, "IR generation failed!\n");
        }
      }
      else
      {
        get_next();
      }
    }

    void handle_top()
    {
      if(auto p = parse_top())
      {
        if(PRINT_DEBUG_FLAG)
        {
          fprintf(stderr, "Parsed a top-level expression:\n\n");
          p -> prettyprint(); fprintf(stderr, "\n");
        }
        if(auto * ir = p->codegen(g))
        {
          if(PRINT_IR_FLAG)
          {
            fprintf(stderr, "Intermediate Representation:\n");
            ir->print(llvm::errs());
            fprintf(stderr, "\n");
          }
          auto H = g.jit_module();
          auto ES = g.jit->findSymbol("__anon_expr");
          assert(ES && "Function not found");

          // Get the symbol's address and cast it to a function pointer
          double (*generated_function)() =
          reinterpret_cast<double (*)()>(
            static_cast<intptr_t>(ES.getAddress().get())
          );

          fprintf(stderr, "%lf\n", generated_function());

          g.jit->removeModule(H);
        }
        else
        {
          fprintf(stderr, "IR generation failed!\n");
        }
      }
      else
      {
        get_next();
      }
    }

  public:

    void set_binary_precedence(char op, int prec)
    {
      binary_precedence[op] = prec;
    }

    void del_binary_precedence(char op)
    {
      binary_precedence.erase(binary_precedence.find(op));
    }

    void run_parser(FILE* output = stderr)
    {
      // Prime first token
      if(get_input() == stdin)
      {
        fprintf(output, "ready>>> ");
      }
      get_next();
      // Interpreter loop
      while(true)
      {
        switch(get_curr())
        {
          case tok_eof:
            return;
          case ';':
            get_next(); // Eat top-level semicolons
            break;
          case tok_def:
            handle_def();
            break;
          case tok_extern:
            handle_extern();
            break;
          default:
            handle_top();
            break;
        }
        if(get_input() == stdin)
        {
          fprintf(output, "ready(%c,%d)>>> ", get_curr(), get_curr());
        }
        get_next();
      }
    }

    Parser(FILE* input): Lexer(input) {}
    Parser(): Lexer() {}
  };

  std::unique_ptr<expr> Parser::parse_expr()
  {
    //fprintf(stderr, "\nAttempting to parse expression (%c,%d)...\n", get_curr(), get_curr());
    auto lhs = parse_unary();
    if(!lhs) {return nullptr;}
    return parse_bin_rhs(0, std::move(lhs));
  }

}

#endif
