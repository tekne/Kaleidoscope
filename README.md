An implementation of a slightly modified version of the Kaleidoscope language based off the tutorial found at https://llvm.org/docs/tutorial/.

Depends on LLVM 5.0 and Clang 5.0. To try the language out, compile with ``make``, then
run ``kaleidoscope`` and type in ``import mandel.ks; mandel(-2.3, -1.3, 0.05, 0.07);``.

Copyright (C) 2018, Jad Elkhaleq Ghalayini, licensed under the MIT license.
See LICENSE.TXT for details. Significant portions adapted from LLVM tutorial
materials, which are released under the University of Illinois Open Source
License. See LICENSE.TXT for details.
